
package com.mycompany.securecoding_example_code;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import java.io.Console;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.Scanner;


/**
 *
 * @author Carlton Davis
 */

/* Class indicating code that are noncompliant and compliant to good
   secure coding practices
*/
public class SecureCodingEx {
    
    //Setup a Logger for the class
    private static final Logger LOGGER = Logger.getLogger(SecureCodingEx.class.getName());
    
    
    //This method is non-compliant to secure coding practices
    private void nonCompliantEx(String str) {
        
        System.out.println("This is the string BEFORE normalization: " + str);
 
        /*Validation involves checking for angular brackets,
          assume security policy doesn't allow angular brackets as input
        */
        Pattern pattern = Pattern.compile("[<>]");
        
        //Search for occurance "<" or "<" within the string str 
        Matcher matcher = pattern.matcher(str);
        
        if (matcher.find()) {         
            // Found black listed tag
            LOGGER.log(Level.SEVERE, "Black listed character found in input");
            throw new IllegalStateException();
        
        } else {
            System.out.println("Input string is acceptable");
        }
 
        /*  \uFE64 is normalized to < and \uFE65 is normalized to > using 
            the NFKC normalization form
        */
        str = Normalizer.normalize(str, Form.NFKC);
        System.out.println("This is the string AFTER normalization: " + str);   
    }
    
    
    //This method is compliant to secure coding practices
    private void compliantEx(String str) {
        
        System.out.println("This is the string BEFORE normalization: " + str);
        
         /*  \uFE64 is normalized to < and \uFE65 is normalized to > using 
            the NFKC normalization form
        */
        str = Normalizer.normalize(str, Form.NFKC);
        System.out.println("This is the string AFTER normalization: " + str); 
 
        /*Validation involves checking for angular brackets,
          assume security policy doesn't allow angular brackets as input
        */
        Pattern pattern = Pattern.compile("[<>]");
        
        //Search for occurance "<" or "<" within the string str 
        Matcher matcher = pattern.matcher(str);
        
        if (matcher.find()) {         
            // Found black listed tag
            LOGGER.log(Level.SEVERE, "Black listed character found in input!!");
        
        } else {
            System.out.println("Input string is acceptable");
        }  
    }
    
    
    //This method accepts only alphanumeric characters
    private void alphanumericInput(String str){
      
       //Sanitized string
       String sanitizedStr = "";
       
       System.out.println("String BEFORE normalization: " + str);
       
       //Normalize the string using the NFKC normalization form
       str = Normalizer.normalize(str, Form.NFKC);
       System.out.println("String AFTER normalization: " + str);
       
       if (Pattern.matches("[a-zA-Z0-9]", str)) {
           sanitizedStr = str;
           System.out.println("Acceptable input");
           
       } else {
           LOGGER.log(Level.SEVERE, "Unacceptable input!!");
       }
       
    }
    
    
    /* Non-compliant method for obtaining username and password from user.
       Return 0 if all is well, and 1 otherwise
    */
    private int getCredentialsNC() {
    
      int returnValue = 0;
      
      Console console = System.console();
      if (console == null) {
          System.out.println("No console available");
          returnValue = 1;
          
      } else { //Get username
          String userName = console.readLine("Enter username: ");
          System.out.println("Username entered: " + userName);
          
          //Non-Compliant way to get password
          String passwd = console.readLine("Enter password: ");
          System.out.println("Password entered: " + passwd);
      }
      
      return returnValue;
    }
    
    
    /* Method for obtaining username and password, which conforms to good 
       secure coding practices.
    */
    private String getCredentialsC() {
    
      String returnStr = "";
      
      Console console = System.console();
      if (console == null) {
          System.out.println("No console available");
          
      } else { //Get username
          String userName = console.readLine("Enter username: ");
          System.out.println("Username entered: " + userName);
          
          // Create instance of Argon2 to hash user password
          Argon2 argon2 = Argon2Factory.create();
          
          //Compliant way to get password from user
          char[] passwd = console.readPassword("Enter password: ");
          
          try {
               /*Use Argon2 library to generate hash of the password.
                See https://github.com/phxql/argon2-jvm  
               */ 
               returnStr = argon2.hash(10, 65536, 1, passwd);
               
          } finally {
              // Wipe confidential data
               argon2.wipeArray(passwd);
          }
      }
      return returnStr;
    }
    
    
    //This method doesn't do input validation: evidently problematic
    private void noInputValidation() {
        //List for storing the animals
        List<String> animals = List.of("Lion", "Tiger", "Leopard", "cheetah");

        System.out.print("\nEnter a number to select one of the animals: ");
        Scanner theScanner = new Scanner(System.in);
        int choice = theScanner.nextInt();

        String animal = animals.get(choice);
        System.out.println("Choice: " + choice + " is " + animal);   
    }
    
    //This method does input validation
    private void doesInputValidation() {
        //List for storing the animals
        List<String> animals = List.of("Lion", "Tiger", "Leopard", "cheetah");
        
        boolean flag;
        String choice;
        
        Scanner theScanner = new Scanner(System.in);
        
        do {
            System.out.print("\n[In doesInputValidation] Enter a number to select one of the animals: ");
            choice = theScanner.next();
            if(!isValid(choice)) {
                System.out.println("Input must be a digit between 0 and 3, inclusively!");
            } 
        } while(!isValid(choice));

        String animal = animals.get(Integer.parseInt(choice));
        System.out.println("Choice: " + choice + " is " + animal); 
    }
    
    
    //Determine if an input is valid
    private boolean isValid(String input) {
        int num;
        
        try {
              //If input is not numeric, NumberFormatException will be thrown
              num = Integer.parseInt(input);
            } catch (NumberFormatException nfe) {
                return false;
            }
        
        
        if (!(num > -1) || !(num < 4)) {
            return false;
        }
        
        return true;
    }

    //Driver code
    public static void main(String[] args) {
        
      SecureCodingEx myObject = new  SecureCodingEx();
      
      String str = "\uFE64" + "script" + "\uFE65";
      
      System.out.println("\nResult for NON-COMPLIANT method:");
      myObject.nonCompliantEx(str);
      
      System.out.println("\nResult for COMPLIANT method:");
      myObject.compliantEx(str);
      
      String str2 = "Input with spaces";
      System.out.println("\nResult for alphanumericInput method:");
      myObject.alphanumericInput(str2);
      
      System.out.println("\nNON-COMPLIANT method for getting user credential");
        int returnValue = myObject.getCredentialsNC();
      
      System.out.println("\nCOMPLIANT method for getting user credential"); 
      String passwdHash =  myObject.getCredentialsC();
      System.out.println("Password hash: " + passwdHash);
      
      myObject.noInputValidation();
      myObject.doesInputValidation();
    }
}
