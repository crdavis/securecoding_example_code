Example code showing compliant vs non-compliant secure coding practices.

Note: The console cann't be accessed from non-interactive environment such as within IDEs. 
To access the console, the code needs to be ran from the command line. If Maven is installed 
on your computer, you can use the following command to run the code from the command line:

mvn compile  \
mvn exec:java -Dexec.mainClass=com.mycompany.securecoding_example_code.SecureCodingEx
